class TaskCard extends HTMLElement {
  constructor() {
    super()
  
    this.taskDescription
    this.attachShadow({mode: 'open'})
  }
  
  connectedCallback() {
    this.render()
  }
  
  static get observedAttributes() {
    return ['task-description']
  }
  
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'task-description') {
    }
    
    this.render()
  }
  
  render() {
    this.shadowRoot.innerHTML = `
    <style>
    div {
    background-color: lightgrey;
    cursor: pointer;
    border-radius: 5px;
    }
    h3 {
    padding: 10px;
    }
    </style>
    
    <div>
    <h3>${this.taskDescription}</h3>
    </div>
    `
  }
  
  get taskDescription() {
    return this.getAttribute('task-description')
  }
}

customElements.define('task-card', TaskCard)