# Trello-like Application - Front-End assignment

**Front-End assignment on Web Components.**

- pull repo
- cd web_components_trello (go to the root folder)
- npm install
- npm start
- go to localhost:8000 in Chrome Browser

- (You might need to do npm install **http-server -g** if above does not work)