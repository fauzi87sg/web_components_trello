class TrelloApp extends HTMLElement {
  constructor() {
    super()
    
    this.columnName = null
    this.columns
    this.countId = 0
    
    this.attachShadow({ mode: 'open' })
  }
  
  connectedCallback() {
    if (!this.columns || this.columns.length === 0) {
    fetch('./materials/db.json').then(res => res.json())
      .then(data => {
        this.columns = this.organiseColumnsData(data)
        this.countId = this.columns[this.columns.length - 1].id + 1
        this.render()
        this.submitBtnListener()
        this.updateColumn()
        
      })
      .catch(err => {
        throw err
      })
      return
    }
    this.render()
    this.submitBtnListener()
    this.updateColumn()
  }
  
  
  organiseColumnsData(data) {
    if (!data) return
    let organiseData = data.columns.map(column => {
      return {
        id: column.id,
        title: column.title,
      }
    })
    
    return organiseData
  }
  
  submitBtnListener() {
    let submitBtn = this.shadowRoot.querySelector('button')
    submitBtn.addEventListener('click', () => {
      this.onCreateColumn()
    })
  }
  
  onCreateColumn() {
    let columnName = this.shadowRoot.querySelector('input').value
    if (columnName === '') return
    this.shadowRoot.querySelector('input').value = ''
    this.columnName = columnName
    this.columns.push({
      id: this.countId++,
      title: this.columnName
    })
    
    this.render()
    this.updateColumn()
  }
  
  updateColumn() {
    this.columns.forEach(column => {
      let selectedColumn = this.shadowRoot.querySelector(`.column-${column.id}`)
      selectedColumn.setAttribute('column-title', column.title)
    })
    this.submitBtnListener()
  }
  
  render() {
    this.shadowRoot.innerHTML = `
    <style>
    .task-div {
    display: inline;
    top: 0;
    height: 1000px;
    }
    </style>
    <div>
    <input type="text" name="columnName" placeholder="Create Column"/>
    <button type="button" value="Submit">Submit</button>
    </div>
    <div>
    ${this.columns ? this.columns.map(column => `<div class="task-div"><task-column class="column-${column.id}"></task-column></div>`).join('') : ''}
    </div>
      `;
  }
}

customElements.define('trello-app', TrelloApp)