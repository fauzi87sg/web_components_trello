class TaskColumn extends HTMLElement {
  constructor() {
    super()
    
    this.columnTitle
    this.columnId = this.getAttribute('class')
    this.tasks = []
    this.countTaskId = 1
    this.attachShadow({ mode: 'open' })
  }
  
  connectedCallback() {
    if (!this.tasks || this.tasks.length === 0) {
      fetch('./materials/db.json').then(res => res.json())
        .then(data => {
          this.tasks = this.organiseTasksData(data)
          this.countTaskId = this.tasks.length > 0 ? this.tasks[this.tasks.length - 1].id + 1 : 1
          this.render()
          this.createBtnListener()
          this.updateTask()
        
        })
        .catch(err => {
          throw err
        })
      return
    }
    
    this.render()
    this.createBtnListener()
    this.updateTask()
  }
  
  static get observedAttributes() {
    return ['column-title', 'card-details']
  }
  
  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'column-title' || name === 'card-details') {
    }
    
    this.render()
    this.createBtnListener()
  }
  
  organiseTasksData(data) {
    if (!data) return
    
    // get column
    let id = this.columnId.split('-')[1]
    let organiseData = data.cards.filter(card => card.columnId === parseInt(id))
    
    return organiseData
  }
  
  createBtnListener() {
    let createBtn = this.shadowRoot.querySelector('.createTaskBtn')
    createBtn.addEventListener('click', () => {
      this.onCreateTask()
    })
  }
  
  onCreateTask() {
    let taskDescription = this.shadowRoot.querySelector(`textarea[name="${this.columnId}"]`).value
    if (taskDescription === '') return
    this.shadowRoot.querySelector(`textarea[name="${this.columnId}"]`).value = ''
    this.tasks.push({
      id: this.countTaskId++,
      description: taskDescription
    })
    
    this.render()
    this.updateTask()
  }
  
  updateTask() {
    this.tasks.forEach(task => {
      this.shadowRoot.querySelector(`.task-${task.id}`).setAttribute('task-description', task.description)
    })
    this.createBtnListener()
  }
  
  render() {
    this.shadowRoot.innerHTML = `
    <style>
    :host {
    margin: 2px;
    display: inline-grid;
    padding: 30px;
    background: rgba(0,0,0,0.1);
    text-align: center;
    width: calc(25% - 4px);
    box-sizing: border-box;
    }
    
    h1, p {
    margin: 10px;
    padding: 0;
    font-family: 'Roboto';
    }
    
    h1 {
    font-size: 1.4em;
    color: #2196f3;
    }
    
    </style>
    <div>
    <h1>${this.columnTitle}</h1>
    </div>
    <div>
    <textarea name="${this.columnId}" cols="35" rows="3" placeholder="Create Task"></textarea>
    <button class="createTaskBtn" type="button" value="Submit">Create</button>
    </div>
    <div>
    ${this.tasks ? this.tasks.map(task => `<task-card class="task-${task.id}"></task-card>`).join('') : ''}
    </div>
    
    `
  }
  
  get columnTitle() {
    return this.getAttribute('column-title')
  }
  
}

customElements.define('task-column', TaskColumn)